package com.cj.st.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserServiceImpl {

    public static String baseUrl = "http://dorm-service-impl";

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getUserNameFallback")
    public String getUserName() {
        System.out.println("将通过restTemplate去调用不同的服务实现getUserName");
        String result = restTemplate.getForObject(baseUrl + "/user/getUserName", String.class);
        System.out.println(result);
        return result;
    }

    @HystrixCommand(fallbackMethod = "getAgeFallback")
    public Integer getAge(Integer age) {
        System.out.println("将通过restTemplate去调用不同的服务实现getAge");
        Integer result = restTemplate.getForObject(baseUrl + "/user/getAge?age=" + age, Integer.class);
        System.out.println(result);
        return result;
    }

    public String getUserNameFallback() {
        System.out.println("===========this is getUserNameFallback");
        return "this is getUserNameFallback";
    }

    public Integer getAgeFallback(Integer age) {
        System.out.println("===========this is getAgeFallback");
        return 0;
    }

}
