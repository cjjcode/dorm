package com.cj.st.controller;

import com.cj.st.UserService;
import com.cj.st.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServiceController implements UserService {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public String getUserName() {
        System.out.println("=========ribbon 接收到请求：getUserName,将通过负载均衡调用服务提供者 ");
        return userService.getUserName();
    }

    @Override
    public Integer getAge(Integer age) {
        System.out.println("=========ribbon 接收到请求，将通过负载均衡调用服务提供者：getAge param:" + age);
        return userService.getAge(age);
    }
}
