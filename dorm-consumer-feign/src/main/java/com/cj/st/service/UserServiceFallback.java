package com.cj.st.service;

import org.springframework.stereotype.Component;

@Component
public class UserServiceFallback implements UserServiceClient {
    @Override
    public String getUserName() {
        System.out.println("=========UserServiceFallback=======getUserName====");
        return "this is fallback";
    }

    @Override
    public Integer getAge(Integer age) {
        System.out.println("=========UserServiceFallback======getAge=====");
        return 0;
    }
}
