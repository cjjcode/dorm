package com.cj.st.controller;

import com.cj.st.UserService;
import com.cj.st.service.UserServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServiceController implements UserService {

    @Autowired
    private UserServiceClient serviceClient;

    public String getUserName() {
        System.out.println("=========feign 接收到请求：getUserName,将通过负载均衡调用服务提供者 ");
        return serviceClient.getUserName();
    }

    public Integer getAge(Integer age) {
        System.out.println("=========feign 接收到请求，将通过负载均衡调用服务提供者：getAge param:" + age);
        return serviceClient.getAge(age);
    }
}
