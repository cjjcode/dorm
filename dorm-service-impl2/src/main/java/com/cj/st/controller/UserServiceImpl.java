package com.cj.st.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServiceImpl {

    @GetMapping("/user/getUserName")
    public String getUserName() {
        System.out.println("this is dorm-service-impl2>>>>>>");
        System.out.println("=========提供各种不同服务：getUserName ");
        return "this is username";
    }

    @GetMapping("/user/getAge")
    public Integer getAge(@RequestParam Integer age) {
        System.out.println("this is dorm-service-impl2>>>>>>");
        System.out.println("=========提供各种不同服务：getAge param:" + age);
        return age;
    }
}
