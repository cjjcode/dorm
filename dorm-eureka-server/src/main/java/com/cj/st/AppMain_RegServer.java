package com.cj.st;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AppMain_RegServer {

    public static void main(String[] args) {

        SpringApplication.run(AppMain_RegServer.class);
    }
}
