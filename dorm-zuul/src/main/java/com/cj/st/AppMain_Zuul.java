package com.cj.st;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@EnableDiscoveryClient
public class AppMain_Zuul {

    public static void main(String[] args) {

        SpringApplication.run(AppMain_Zuul.class);
    }
}
//访问 http://localhost:9001/api-ribbon/user/getUserName
//http://localhost:9001/api-ribbon/user/getAge?age=8