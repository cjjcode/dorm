package com.cj.st;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface UserService {

    @GetMapping("/user/getUserName")
    public String getUserName();

    @GetMapping("/user/getAge")
    public Integer getAge(@RequestParam Integer age);
}
